# No 1
1. Klik 2x pada file instalasi yang bisa didownload di link berikut https://www.enterprisedb.com/downloads/postgres-postgresql-downloads, lalu klik next

![1](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/1.png)

2. Pilih lokasi folder untuk menginstall PostgreSql, lalu klik next

![2](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/2.png)

3. Pilih komponen yang akan diinstall, lalu klik next

![3](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/3.png)

4. Pilih lokasi folder untuk store data, lalu klik next

![4](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/4.png)

5. Buat password untuk database, lalu klik next

![5](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/5.png)

6. Pilih port number, lalu klik next

![6](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/6.png)

7. Pilih lokasi, lalu klik next

![7](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/7.png)

8. Disini akan diberi tahu hal hal yang sudah dikonfirmasi sebelumnya, lalu klik next

![8](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/8.png)

9. Sekarang sudah siap diinstall, klik next untuk melanjutkan

![9](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/9.png)

10. Tunggu sampai proses instalasi selesai

![10](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/10.png)

11. PostgreSql tealh diinstall di komputer/laptop anda

![11](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Instalasi%20PostgreSQL/11.png)

# No 2
1. Buka pg admin4, lalu masukkan password yang tadi anda Buat

![13](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/13.png)

2. Buka server lalu masukkan kembali passwordnya

![14](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/14.png)

3. Buka DBeaver, lalu klik lambang hati di kiri atas

![16](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/16.png)

4. Pilih PostgreSql

![17](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/17.png)

5. Sesuaikan port yang tadi anda telah masukkan, lalu masukkan passwordnya, setelah itu klik test connection

![18](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/18.png)

6. Jika anda belum menginstall driver yang diperlukan maka install terlebih dahulu

![19](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/19.png)

7. Setelah selesai klik ok, lalu finish

![20](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/Konfigurasi/20.png)

# No 3
![bukti](https://gitlab.com/Dikito08/praktikum-basis-data/-/raw/main/Instalasi/21.png)
